﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Org.Openaq.Ap.Openaq.Interfaces.Api;
using Org.Openaq.Ap.Openaq.Interfaces.Models.Responses;

namespace Org.Openaq.Api.Openaq.Api
{
    public class Proxy : IProxy
    {

        private ICountriesApi Countries { get; set; }

        private ICitiesApi Cities { get; set; }

        private IFetchesApi Fetches { get; set; }

        private ILatestApi Latest { get; set; }

        private ILocationsApi Locations { get; set; }

        private IMeasurementsApi Measurements { get; set; }

        private ISourcesApi Sources { get; set; }

        private IParametersApi Parameters { get; set; }

        public Proxy(String basePath = null)
        {
            Countries = new CountriesApi(basePath);
            Fetches = new FetchesApi(basePath);
            Cities = new CitiesApi(basePath);
            Latest = new LatestApi(basePath);
            Locations = new LocationsApi(basePath);
            Measurements = new MeasurementsApi(basePath);
            Parameters= new ParametersApi(basePath);
            Sources = new SourcesApi(basePath);
        }
        #region ICityApi

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Get
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="country">Limit results by a certain country. (optional)</param>
        /// <param name="orderBy">Order by one or more fields (ex. order_by&#x3D;country or order_by[]&#x3D;country&amp;order_by[]&#x3D;locations). (optional, default to country)</param>
        /// <param name="sort">sortDefine sort order for one or more fields (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100)</param>
        /// <param name="page">Paginate through results. (optional, default to 1)</param>
        /// <returns>List&lt;City&gt;</returns>
        public ICityResponse GetCities(string country = null, string orderBy = null, string sort = null, int? limit = null, int? page = null)
        {
            return Cities.GetCities(country, orderBy, sort, limit, page);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// Get
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="country">Limit results by a certain country. (optional)</param>
        /// <param name="orderBy">Order by one or more fields (ex. order_by&#x3D;country or order_by[]&#x3D;country&amp;order_by[]&#x3D;locations). (optional, default to country)</param>
        /// <param name="sort">sortDefine sort order for one or more fields (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100)</param>
        /// <param name="page">Paginate through results. (optional, default to 1)</param>
        /// <returns>Task of List&lt;City&gt;</returns>
        public Task<ICityResponse> GetCitiesAsync(string country = null, string orderBy = null, string sort = null,
            int? limit = null, int? page = null)
        {
            return Cities.GetCitiesAsync(country, orderBy, sort, limit, page);
        }


        #endregion ICityApi
        #region ICountriesApi
        
        /// <summary>
        /// Provides a simple listing of countries within the platform
        /// </summary>
        /// <remarks>
        /// https://api.openaq.org/v1/countries
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="orderBy">order_by (optional)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc). (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100)</param>
        /// <param name="page">pagePaginate through results. (optional, default to 1)</param>
        /// <returns>List&lt;Country&gt;</returns>
        public ICountryResponse GetCountries(List<string> orderBy = null, List<string> sort = null, int? limit = null,
            int? page = null)
        {
            return Countries.GetCountries(orderBy, sort, limit, page);
        }

       
        /// <summary>
        /// Provides a simple listing of countries within the platform
        /// </summary>
        /// <remarks>
        /// https://api.openaq.org/v1/countries
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="orderBy">order_by (optional)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc). (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100)</param>
        /// <param name="page">pagePaginate through results. (optional, default to 1)</param>
        /// <returns>Task of List&lt;Country&gt;</returns>
        public Task<ICountryResponse> GetCountriesAsync(List<string> orderBy = null, List<string> sort = null,
            int? limit = null, int? page = null)
        {
            return Countries.GetCountriesAsync(orderBy, sort, limit, page);
        }
        #endregion ICountriesApi
        #region IFetchesAPi

        /// <summary>
        /// Providing data about individual fetch operations that are used to populate data in the platform.
        /// </summary>
        /// <remarks>
        /// Providing data about individual fetch operations that are used to populate data in the platform.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="orderBy">order_by (optional)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc). (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100)</param>
        /// <param name="page">Paginate through results. (optional, default to 1)</param>
        /// <returns>Fetch</returns>
        public IFetchResponse GetFectches(List<string> orderBy = null, List<string> sort = null,
            int? limit = null, int? page = null)
        {
            return Fetches.GetFectches(orderBy, sort, limit, page);
        }


        /// <summary>
        /// Providing data about individual fetch operations that are used to populate data in the platform.
        /// </summary>
        /// <remarks>
        /// Providing data about individual fetch operations that are used to populate data in the platform.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="orderBy">order_by (optional)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc). (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100)</param>
        /// <param name="page">Paginate through results. (optional, default to 1)</param>
        /// <returns>Task of Fetch</returns>
        public Task<IFetchResponse> GetFectchesAsync(List<string> orderBy = null, List<string> sort = null,
            int? limit = null, int? page = null)
        {
            return Fetches.GetFectchesAsync(orderBy, sort, limit, page);
        }

        #endregion IFetchesAPi
        #region ILatestApi


        /// <summary>
        /// Provides the latest value of each available parameter for every location in the system.
        /// </summary>
        /// <remarks>
        /// Provides the latest value of each available parameter for every location in the system.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="city">Limit results by a certain city. (optional)</param>
        /// <param name="country">Limit results by a certain country. (optional)</param>
        /// <param name="location">Limit results by a certain location. (optional)</param>
        /// <param name="parameter">parameter (optional)</param>
        /// <param name="hasGeo">Filter out items that have or do not have geographic information. (optional)</param>
        /// <param name="coordinates">Center point (lat, lon) used to get measurements within a certain area. (ex. coordinates&#x3D;40.23,34.17) Will add distance property. (optional)</param>
        /// <param name="radius">Radius (in meters) used to get measurements within a certain area, must be used with coordinates. (optional, default to 2500)</param>
        /// <param name="orderBy">Order by one or more fields (optional, default to location)</param>
        /// <param name="sort">Define sort order for one or more fields (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100)</param>
        /// <param name="page">aginate through results. (optional, default to 1)</param>
        /// <returns>Parameters</returns>
        public ILatestResponse GetLatestParameters(string city = null, string country = null,
            string location = null, string parameter = null, bool? hasGeo = null, string coordinates = null,
            int? radius = null, List<string> orderBy = null, List<string> sort = null, int? limit = null,
            int? page = null)
        {
            return Latest.GetLatestParameters(city, country, location, parameter, hasGeo, coordinates, radius, orderBy,
                sort, limit, page);
        }


        /// <summary>
        /// Provides the latest value of each available parameter for every location in the system.
        /// </summary>
        /// <remarks>
        /// Provides the latest value of each available parameter for every location in the system.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="city">Limit results by a certain city. (optional)</param>
        /// <param name="country">Limit results by a certain country. (optional)</param>
        /// <param name="location">Limit results by a certain location. (optional)</param>
        /// <param name="parameter">parameter (optional)</param>
        /// <param name="hasGeo">Filter out items that have or do not have geographic information. (optional)</param>
        /// <param name="coordinates">Center point (lat, lon) used to get measurements within a certain area. (ex. coordinates&#x3D;40.23,34.17) Will add distance property. (optional)</param>
        /// <param name="radius">Radius (in meters) used to get measurements within a certain area, must be used with coordinates. (optional, default to 2500)</param>
        /// <param name="orderBy">Order by one or more fields (optional, default to location)</param>
        /// <param name="sort">Define sort order for one or more fields (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100)</param>
        /// <param name="page">aginate through results. (optional, default to 1)</param>
        /// <returns>Task of Parameters</returns>
        public Task<ILatestResponse> GetLatestParametersAsync(string city = null, string country = null,
            string location = null, string parameter = null, bool? hasGeo = null, string coordinates = null,
            int? radius = null, List<string> orderBy = null, List<string> sort = null, int? limit = null,
            int? page = null)
        {
            return Latest.GetLatestParametersAsync(city, country, location, parameter, hasGeo, coordinates, radius, orderBy,
                sort, limit, page);
        }

        #endregion LatestApi
        #region ILocationsApi

        /// <summary>
        /// Provides a list of measurement locations and their meta data.
        /// </summary>
        /// <remarks>
        /// Provides a list of measurement locations and their meta data.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="city">Limit results by one or more cities (ex. city[]&#x3D;Lisboa&amp;city[]&#x3D;Porto)     (optional)</param>
        /// <param name="country">Limit results by one or more countries (ex. country[]&#x3D;NL&amp;country[]&#x3D;PL)     (optional)</param>
        /// <param name="location">Limit results by one or more locations (ex. location[]&#x3D;Reja&amp;location[]&#x3D;Nijmegen-Graafseweg)     (optional)</param>
        /// <param name="parameter">Limit to certain one or more parameters (ex. parameter&#x3D;pm25 or parameter[]&#x3D;co&amp;parameter[]&#x3D;pm25)      (optional)</param>
        /// <param name="hasGeo">Filter out items that have or do not have geographic information.     (optional)</param>
        /// <param name="coordinates">Center point (lat, lon) used to get locations within/near a certain area. (ex. coordinates&#x3D;40.23,34.17)     (optional)</param>
        /// <param name="nearest">Get the X nearest locations to coordinates, must be used with coordinates. Wins over radius if both are present. Will add distance property to locations. DEPRECATED: Use order_by&#x3D;distance with limit&#x3D;X instead.     (optional)</param>
        /// <param name="radius">Radius (in meters) used to get locations within a certain area, must be used with coordinates.  (optional, default to 2500.0)</param>
        /// <param name="orderBy">Order by one or more fields (ex. order_by&#x3D;count or order_by[]&#x3D;country&amp;order_by[]&#x3D;count). (optional, default to location)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc).     Valor por defecto: asc     (optional)</param>
        /// <param name="limit">Change the number of results returned     Valor por defecto: 100     (optional, default to 100.0)</param>
        /// <param name="page">Paginate through results.    (optional, default to 1.0)</param>
        /// <returns>List&lt;Locations&gt;</returns>
        public ILocationResponse GetLocations(string city = null, string country = null, string location = null,
            string parameter = null, bool? hasGeo = null, string coordinates = null, float? nearest = null,
            float? radius = null, List<string> orderBy = null, List<string> sort = null, float? limit = null,
            float? page = null)
        {
            return Locations.GetLocations(city, country, location, parameter, hasGeo, coordinates, nearest, radius,
                orderBy, sort, limit, page);
        }

       

        /// <summary>
        /// Provides a list of measurement locations and their meta data.
        /// </summary>
        /// <remarks>
        /// Provides a list of measurement locations and their meta data.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="city">Limit results by one or more cities (ex. city[]&#x3D;Lisboa&amp;city[]&#x3D;Porto)     (optional)</param>
        /// <param name="country">Limit results by one or more countries (ex. country[]&#x3D;NL&amp;country[]&#x3D;PL)     (optional)</param>
        /// <param name="location">Limit results by one or more locations (ex. location[]&#x3D;Reja&amp;location[]&#x3D;Nijmegen-Graafseweg)     (optional)</param>
        /// <param name="parameter">Limit to certain one or more parameters (ex. parameter&#x3D;pm25 or parameter[]&#x3D;co&amp;parameter[]&#x3D;pm25)      (optional)</param>
        /// <param name="hasGeo">Filter out items that have or do not have geographic information.     (optional)</param>
        /// <param name="coordinates">Center point (lat, lon) used to get locations within/near a certain area. (ex. coordinates&#x3D;40.23,34.17)     (optional)</param>
        /// <param name="nearest">Get the X nearest locations to coordinates, must be used with coordinates. Wins over radius if both are present. Will add distance property to locations. DEPRECATED: Use order_by&#x3D;distance with limit&#x3D;X instead.     (optional)</param>
        /// <param name="radius">Radius (in meters) used to get locations within a certain area, must be used with coordinates.  (optional, default to 2500.0)</param>
        /// <param name="orderBy">Order by one or more fields (ex. order_by&#x3D;count or order_by[]&#x3D;country&amp;order_by[]&#x3D;count). (optional, default to location)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc).     Valor por defecto: asc     (optional)</param>
        /// <param name="limit">Change the number of results returned     Valor por defecto: 100     (optional, default to 100.0)</param>
        /// <param name="page">Paginate through results.    (optional, default to 1.0)</param>
        /// <returns>Task of List&lt;Locations&gt;</returns>
        public Task<ILocationResponse> GetLocationsAsync(string city = null, string country = null,
            string location = null, string parameter = null, bool? hasGeo = null, string coordinates = null,
            float? nearest = null, float? radius = null, List<string> orderBy = null, List<string> sort = null,
            float? limit = null, float? page = null)
        {
            return Locations.GetLocationsAsync(city, country, location, parameter, hasGeo, coordinates, nearest, radius,
                orderBy, sort, limit, page);

        }
        #endregion ILocationsApi
        #region  IMeasurementsApi

        /// <summary>
        /// Provides data about individual measurements
        /// </summary>
        /// <remarks>
        /// Provides data about individual measurements
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="country">    Limit results by a certain country.     (optional)</param>
        /// <param name="city">    Limit results by a certain city.     (optional)</param>
        /// <param name="location">    Limit results by a certain location.     (optional)</param>
        /// <param name="parameter">    Limit to certain one or more parameters (ex. parameter&#x3D;pm25 or parameter[]&#x3D;co&amp;parameter[]&#x3D;pm25)     Valores permitidos: pm25, pm10, so2, no2, o3, co, bc     (optional)</param>
        /// <param name="hasGeo">    Filter out items that have or do not have geographic information.     Valores permitidos: true, false     (optional)</param>
        /// <param name="coordinates">    Center point (lat, lon) used to get measurements within a certain area. (ex. coordinates&#x3D;40.23,34.17)     (optional)</param>
        /// <param name="radius">    Radius (in meters) used to get measurements within a certain area, must be used with coordinates.     Valor por defecto: 2500     (optional, default to 2500.0)</param>
        /// <param name="valueFrom">    Show results above value threshold, useful in combination with parameter.     (optional)</param>
        /// <param name="valueTo">    Show results below value threshold, useful in combination with parameter.     (optional)</param>
        /// <param name="dateFrom">    Show results after a certain date. This acts on the utc timestamp of each measurement. (ex. 2015-12-20, or 2015-12-20T09:00:00)     (optional)</param>
        /// <param name="dateTo">    Show results before a certain date. This acts on the utc timestamp of each measurement. (ex. 2015-12-20, or 2015-12-20T09:00:00)     (optional)</param>
        /// <param name="orderBy">    Order by one or more fields (ex. order_by&#x3D;date or order_by[]&#x3D;value&amp;order_by[]&#x3D;parameter).     Valor por defecto: date     (optional, default to date)</param>
        /// <param name="sort">    Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc).     Valor por defecto: asc     (optional, default to asc)</param>
        /// <param name="includeFields">    Include extra fields in the output in addition to default values.     Valores permitidos: attribution, averagingPeriod, sourceName     (optional)</param>
        /// <param name="limit">    Change the number of results returned, max is 10000.     Valor por defecto: 100     (optional, default to 100.0)</param>
        /// <param name="page">    Paginate through results.     Valor por defecto: 1     (optional, default to 1.0)</param>
        /// <param name="format">    Format for data return. Note that csv will return a max of 65,536 results when no limit is set.     Valor por defecto: json     Valores permitidos: csv, json     (optional, default to json)</param>
        /// <returns>Measurements</returns>
        public IMeasurementResponse GetMeasurements(string country = null, string city = null, string location = null,
            string parameter = null, bool? hasGeo = null, string coordinates = null, float? radius = null,
            float? valueFrom = null, float? valueTo = null, string dateFrom = null, string dateTo = null,
            string orderBy = null, string sort = null, List<string> includeFields = null, float? limit = null,
            float? page = null, string format = null)
        {
            return Measurements.GetMeasurements(country, city, location, parameter, hasGeo, coordinates, radius, valueFrom,
                valueTo, dateFrom, dateTo, orderBy, sort, includeFields, limit, page);
        }



        /// <summary>
        /// Provides data about individual measurements
        /// </summary>
        /// <remarks>
        /// Provides data about individual measurements
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="country">    Limit results by a certain country.     (optional)</param>
        /// <param name="city">    Limit results by a certain city.     (optional)</param>
        /// <param name="location">    Limit results by a certain location.     (optional)</param>
        /// <param name="parameter">    Limit to certain one or more parameters (ex. parameter&#x3D;pm25 or parameter[]&#x3D;co&amp;parameter[]&#x3D;pm25)     Valores permitidos: pm25, pm10, so2, no2, o3, co, bc     (optional)</param>
        /// <param name="hasGeo">    Filter out items that have or do not have geographic information.     Valores permitidos: true, false     (optional)</param>
        /// <param name="coordinates">    Center point (lat, lon) used to get measurements within a certain area. (ex. coordinates&#x3D;40.23,34.17)     (optional)</param>
        /// <param name="radius">    Radius (in meters) used to get measurements within a certain area, must be used with coordinates.     Valor por defecto: 2500     (optional, default to 2500.0)</param>
        /// <param name="valueFrom">    Show results above value threshold, useful in combination with parameter.     (optional)</param>
        /// <param name="valueTo">    Show results below value threshold, useful in combination with parameter.     (optional)</param>
        /// <param name="dateFrom">    Show results after a certain date. This acts on the utc timestamp of each measurement. (ex. 2015-12-20, or 2015-12-20T09:00:00)     (optional)</param>
        /// <param name="dateTo">    Show results before a certain date. This acts on the utc timestamp of each measurement. (ex. 2015-12-20, or 2015-12-20T09:00:00)     (optional)</param>
        /// <param name="orderBy">    Order by one or more fields (ex. order_by&#x3D;date or order_by[]&#x3D;value&amp;order_by[]&#x3D;parameter).     Valor por defecto: date     (optional, default to date)</param>
        /// <param name="sort">    Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc).     Valor por defecto: asc     (optional, default to asc)</param>
        /// <param name="includeFields">    Include extra fields in the output in addition to default values.     Valores permitidos: attribution, averagingPeriod, sourceName     (optional)</param>
        /// <param name="limit">    Change the number of results returned, max is 10000.     Valor por defecto: 100     (optional, default to 100.0)</param>
        /// <param name="page">    Paginate through results.     Valor por defecto: 1     (optional, default to 1.0)</param>
        /// <param name="format">    Format for data return. Note that csv will return a max of 65,536 results when no limit is set.     Valor por defecto: json     Valores permitidos: csv, json     (optional, default to json)</param>
        /// <returns>Task of Measurements</returns>
        public Task<IMeasurementResponse> GetMeasurementsAsync(string country = null, string city = null,
            string location = null, string parameter = null, bool? hasGeo = null, string coordinates = null,
            float? radius = null, float? valueFrom = null, float? valueTo = null, string dateFrom = null,
            string dateTo = null, string orderBy = null, string sort = null, List<string> includeFields = null,
            float? limit = null, float? page = null, string format = null)
        {
            return Measurements.GetMeasurementsAsync(country, city, location, parameter, hasGeo, coordinates, radius, valueFrom,
                valueTo, dateFrom, dateTo, orderBy, sort, includeFields, limit, page);
        }

        #endregion IMeasurementsApi
        #region IParametersApi

        /// <summary>
        /// Provides a simple listing of parameters within the platform.
        /// </summary>
        /// <remarks>
        /// Provides a simple listing of parameters within the platform.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="orderBy">rder by one or more fields (ex. order_by&#x3D;name or order_by[]&#x3D;preferredUnit&amp;order_by[]&#x3D;id). (optional, default to name)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc). (optional, default to asc)</param>
        /// <returns>List&lt;Parameters&gt;</returns>
        public IParametersResponse Getparameters(List<string> orderBy = null, List<string> sort = null)
        {
            return Parameters.Getparameters(orderBy, sort);
        }

        /// <summary>
        /// Provides a simple listing of parameters within the platform.
        /// </summary>
        /// <remarks>
        /// Provides a simple listing of parameters within the platform.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="orderBy">rder by one or more fields (ex. order_by&#x3D;name or order_by[]&#x3D;preferredUnit&amp;order_by[]&#x3D;id). (optional, default to name)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc). (optional, default to asc)</param>
        /// <returns>Task of List&lt;Parameters&gt;</returns>
        public Task<IParametersResponse> GetparametersAsync(List<string> orderBy = null, List<string> sort = null)
        {
            return Parameters.GetparametersAsync(orderBy, sort);
        }

        #endregion IParameterApi
        #region ISourcesApi
       

        /// <summary>
        /// Provides a list of data sources.
        /// </summary>
        /// <remarks>
        /// Provides a list of data sources.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="orderBy">Order by one or more fields (ex. order_by&#x3D;country or order_by[]&#x3D;active&amp;order_by[]&#x3D;country). (optional, default to name)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc) (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100.0)</param>
        /// <param name="page">Paginate through results. (optional, default to 1.0)</param>
        /// <returns>List&lt;Sources&gt;</returns>
        public ISourcesResponse GetSources(List<string> orderBy = null, string sort = null, float? limit = null,
            float? page = null)
        {
            return Sources.GetSources(orderBy, sort, limit, page);
        }

        /// <summary>
        /// Provides a list of data sources.
        /// </summary>
        /// <remarks>
        /// Provides a list of data sources.
        /// </remarks>
        /// <exception cref="Org.Openaq.Api.Openaq.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="orderBy">Order by one or more fields (ex. order_by&#x3D;country or order_by[]&#x3D;active&amp;order_by[]&#x3D;country). (optional, default to name)</param>
        /// <param name="sort">Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc) (optional, default to asc)</param>
        /// <param name="limit">Change the number of results returned (optional, default to 100.0)</param>
        /// <param name="page">Paginate through results. (optional, default to 1.0)</param>
        /// <returns>Task of List&lt;Sources&gt;</returns>
        public Task<ISourcesResponse> GetSourcesAsync(List<string> orderBy = null, string sort = null,
            float? limit = null, float? page = null)

        {
            return Sources.GetSourcesAsync(orderBy, sort, limit, page);
        }
        #endregion IsourcesApi
    }
}
