/* 
 * Openaq
 *
 * <p>API for api.openaq.org</p> 
 *
 * OpenAPI spec version: 0.1.0
 * Contact: vicent@galysoft.es
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Org.Openaq.Api.Openaq.Api;
using Org.Openaq.Api.Openaq.Model;
using Org.Openaq.Api.Openaq.Client;
using System.Reflection;

namespace Org.Openaq.Api.Openaq.Test
{
    /// <summary>
    ///  Class for testing City
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class CityTests
    {
        private City instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new City();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of City
        /// </summary>
        [Test]
        public void CityInstanceTest()
        {
            Assert.IsInstanceOf<City> (instance, "instance is a City");
        }

        /// <summary>
        /// Test the property '_City'
        /// </summary>
        [Test]
        public void _CityTest()
        {
            // TODO: unit test for the property '_City'
        }
        /// <summary>
        /// Test the property 'Count'
        /// </summary>
        [Test]
        public void CountTest()
        {
            // TODO: unit test for the property 'Count'
        }
        /// <summary>
        /// Test the property 'Country'
        /// </summary>
        [Test]
        public void CountryTest()
        {
            // TODO: unit test for the property 'Country'
        }
        /// <summary>
        /// Test the property 'Locations'
        /// </summary>
        [Test]
        public void LocationsTest()
        {
            // TODO: unit test for the property 'Locations'
        }

    }

}
