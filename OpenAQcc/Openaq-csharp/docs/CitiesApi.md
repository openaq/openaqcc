# Org.Openaq.Api.Openaq.Api.CitiesApi

All URIs are relative to *https://api.openaq.org/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetCities**](CitiesApi.md#getcities) | **GET** /cities/ | 


# **GetCities**
> List<City> GetCities (string country = null, string orderBy = null, string sort = null, int? limit = null, int? page = null)



Get

### Example
```csharp
using System;
using System.Diagnostics;
using Org.Openaq.Api.Openaq.Api;
using Org.Openaq.Api.Openaq.Client;
using Org.Openaq.Api.Openaq.Model;

namespace Example
{
    public class GetCitiesExample
    {
        public void main()
        {
            
            var apiInstance = new CitiesApi();
            var country = ;  // string | Limit results by a certain country. (optional) 
            var orderBy = ;  // string | Order by one or more fields (ex. order_by=country or order_by[]=country&order_by[]=locations). (optional)  (default to country)
            var sort = ;  // string | sortDefine sort order for one or more fields (optional)  (default to asc)
            var limit = ;  // int? | Change the number of results returned (optional)  (default to 100)
            var page = ;  // int? | Paginate through results. (optional)  (default to 1)

            try
            {
                List&lt;City&gt; result = apiInstance.GetCities(country, orderBy, sort, limit, page);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CitiesApi.GetCities: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **string**| Limit results by a certain country. | [optional] 
 **orderBy** | **string**| Order by one or more fields (ex. order_by&#x3D;country or order_by[]&#x3D;country&amp;order_by[]&#x3D;locations). | [optional] [default to country]
 **sort** | **string**| sortDefine sort order for one or more fields | [optional] [default to asc]
 **limit** | **int?**| Change the number of results returned | [optional] [default to 100]
 **page** | **int?**| Paginate through results. | [optional] [default to 1]

### Return type

[**List<City>**](City.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

