# Org.Openaq.Api.Openaq.Api.LatestApi

All URIs are relative to *https://api.openaq.org/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetLatestParameters**](LatestApi.md#getlatestparameters) | **GET** /latest | Provides the latest value of each available parameter for every location in the system.


# **GetLatestParameters**
> Parameters GetLatestParameters (string city = null, string country = null, string location = null, string parameter = null, bool? hasGeo = null, string coordinates = null, int? radius = null, List<string> orderBy = null, List<string> sort = null, int? limit = null, int? page = null)

Provides the latest value of each available parameter for every location in the system.

Provides the latest value of each available parameter for every location in the system.

### Example
```csharp
using System;
using System.Diagnostics;
using Org.Openaq.Api.Openaq.Api;
using Org.Openaq.Api.Openaq.Client;
using Org.Openaq.Api.Openaq.Model;

namespace Example
{
    public class GetLatestParametersExample
    {
        public void main()
        {
            
            var apiInstance = new LatestApi();
            var city = ;  // string | Limit results by a certain city. (optional) 
            var country = ;  // string | Limit results by a certain country. (optional) 
            var location = ;  // string | Limit results by a certain location. (optional) 
            var parameter = ;  // string | parameter (optional) 
            var hasGeo = ;  // bool? | Filter out items that have or do not have geographic information. (optional) 
            var coordinates = ;  // string | Center point (lat, lon) used to get measurements within a certain area. (ex. coordinates=40.23,34.17) Will add distance property. (optional) 
            var radius = ;  // int? | Radius (in meters) used to get measurements within a certain area, must be used with coordinates. (optional)  (default to 2500)
            var orderBy = new List<string>(); // List<string> | Order by one or more fields (optional)  (default to location)
            var sort = new List<string>(); // List<string> | Define sort order for one or more fields (optional)  (default to asc)
            var limit = ;  // int? | Change the number of results returned (optional)  (default to 100)
            var page = ;  // int? | aginate through results. (optional)  (default to 1)

            try
            {
                // Provides the latest value of each available parameter for every location in the system.
                Parameters result = apiInstance.GetLatestParameters(city, country, location, parameter, hasGeo, coordinates, radius, orderBy, sort, limit, page);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LatestApi.GetLatestParameters: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **city** | **string**| Limit results by a certain city. | [optional] 
 **country** | **string**| Limit results by a certain country. | [optional] 
 **location** | **string**| Limit results by a certain location. | [optional] 
 **parameter** | **string**| parameter | [optional] 
 **hasGeo** | **bool?**| Filter out items that have or do not have geographic information. | [optional] 
 **coordinates** | **string**| Center point (lat, lon) used to get measurements within a certain area. (ex. coordinates&#x3D;40.23,34.17) Will add distance property. | [optional] 
 **radius** | **int?**| Radius (in meters) used to get measurements within a certain area, must be used with coordinates. | [optional] [default to 2500]
 **orderBy** | [**List<string>**](string.md)| Order by one or more fields | [optional] [default to location]
 **sort** | [**List<string>**](string.md)| Define sort order for one or more fields | [optional] [default to asc]
 **limit** | **int?**| Change the number of results returned | [optional] [default to 100]
 **page** | **int?**| aginate through results. | [optional] [default to 1]

### Return type

[**Parameters**](Parameters.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

