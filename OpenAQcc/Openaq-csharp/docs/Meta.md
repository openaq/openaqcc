# Org.Openaq.Api.Openaq.Model.Meta
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Found** | **int?** |  | 
**License** | **string** |  | [optional] 
**Limit** | **int?** |  | [optional] 
**Name** | **string** |  | [optional] 
**Page** | **int?** |  | [optional] 
**Website** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

