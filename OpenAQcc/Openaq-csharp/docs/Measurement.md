# Org.Openaq.Api.Openaq.Model.Measurement
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Attribution** | [**List&lt;Attribution&gt;**](Attribution.md) |  | 
**AveragingPeriod** | [**AveragingPeriod**](AveragingPeriod.md) |  | [optional] 
**City** | **string** |  | [optional] 
**Coordinates** | [**Coordinates**](Coordinates.md) |  | [optional] 
**Country** | **string** |  | [optional] 
**Date** | **DateTime?** |  | [optional] 
**Location** | **string** |  | [optional] 
**Parameter** | **string** |  | [optional] 
**SourceName** | **string** |  | [optional] 
**Unit** | **string** |  | [optional] 
**Value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

