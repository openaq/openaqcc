# Org.Openaq.Api.Openaq.Api.ParametersApi

All URIs are relative to *https://api.openaq.org/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Getparameters**](ParametersApi.md#getparameters) | **GET** /parameters | Provides a simple listing of parameters within the platform.


# **Getparameters**
> List<Parameters> Getparameters (List<string> orderBy = null, List<string> sort = null)

Provides a simple listing of parameters within the platform.

Provides a simple listing of parameters within the platform.

### Example
```csharp
using System;
using System.Diagnostics;
using Org.Openaq.Api.Openaq.Api;
using Org.Openaq.Api.Openaq.Client;
using Org.Openaq.Api.Openaq.Model;

namespace Example
{
    public class GetparametersExample
    {
        public void main()
        {
            
            var apiInstance = new ParametersApi();
            var orderBy = new List<string>(); // List<string> | rder by one or more fields (ex. order_by=name or order_by[]=preferredUnit&order_by[]=id). (optional)  (default to name)
            var sort = new List<string>(); // List<string> | Define sort order for one or more fields (ex. sort=desc or sort[]=asc&sort[]=desc). (optional)  (default to asc)

            try
            {
                // Provides a simple listing of parameters within the platform.
                List&lt;Parameters&gt; result = apiInstance.Getparameters(orderBy, sort);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ParametersApi.Getparameters: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderBy** | [**List<string>**](string.md)| rder by one or more fields (ex. order_by&#x3D;name or order_by[]&#x3D;preferredUnit&amp;order_by[]&#x3D;id). | [optional] [default to name]
 **sort** | [**List<string>**](string.md)| Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc). | [optional] [default to asc]

### Return type

[**List<Parameters>**](Parameters.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

