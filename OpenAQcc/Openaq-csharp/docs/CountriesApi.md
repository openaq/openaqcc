# Org.Openaq.Api.Openaq.Api.CountriesApi

All URIs are relative to *https://api.openaq.org/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetCountries**](CountriesApi.md#getcountries) | **GET** /countries/ | Provides a simple listing of countries within the platform


# **GetCountries**
> List<Country> GetCountries (List<string> orderBy = null, List<string> sort = null, int? limit = null, int? page = null)

Provides a simple listing of countries within the platform

https://api.openaq.org/v1/countries

### Example
```csharp
using System;
using System.Diagnostics;
using Org.Openaq.Api.Openaq.Api;
using Org.Openaq.Api.Openaq.Client;
using Org.Openaq.Api.Openaq.Model;

namespace Example
{
    public class GetCountriesExample
    {
        public void main()
        {
            
            var apiInstance = new CountriesApi();
            var orderBy = new List<string>(); // List<string> | order_by (optional) 
            var sort = new List<string>(); // List<string> | Define sort order for one or more fields (ex. sort=desc or sort[]=asc&sort[]=desc). (optional)  (default to asc)
            var limit = ;  // int? | Change the number of results returned (optional)  (default to 100)
            var page = ;  // int? | pagePaginate through results. (optional)  (default to 1)

            try
            {
                // Provides a simple listing of countries within the platform
                List&lt;Country&gt; result = apiInstance.GetCountries(orderBy, sort, limit, page);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling CountriesApi.GetCountries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderBy** | [**List<string>**](string.md)| order_by | [optional] 
 **sort** | [**List<string>**](string.md)| Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc). | [optional] [default to asc]
 **limit** | **int?**| Change the number of results returned | [optional] [default to 100]
 **page** | **int?**| pagePaginate through results. | [optional] [default to 1]

### Return type

[**List<Country>**](Country.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

