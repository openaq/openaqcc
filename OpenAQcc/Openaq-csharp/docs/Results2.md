# Org.Openaq.Api.Openaq.Model.Results2
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Count** | **int?** |  | 
**Duration** | **float?** |  | [optional] 
**Failures** | [**Failures**](Failures.md) |  | [optional] 
**Message** | **string** |  | [optional] 
**SourceName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

