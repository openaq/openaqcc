# Org.Openaq.Api.Openaq.Model.City
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_City** | **string** |  | 
**Count** | **int?** |  | [optional] 
**Country** | **string** |  | [optional] 
**Locations** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

