# Org.Openaq.Api.Openaq.Model.Locations
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**City** | **string** |  | 
**Count** | **int?** |  | [optional] 
**Country** | **string** |  | [optional] 
**FirstUpdated** | **DateTime?** |  | [optional] 
**LastUpdated** | **DateTime?** |  | [optional] 
**Location** | **string** |  | [optional] 
**Parameters** | **List&lt;string&gt;** |  | [optional] 
**SourceName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

