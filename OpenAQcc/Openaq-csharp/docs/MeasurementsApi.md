# Org.Openaq.Api.Openaq.Api.MeasurementsApi

All URIs are relative to *https://api.openaq.org/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetMeasurements**](MeasurementsApi.md#getmeasurements) | **GET** /measurements | Provides data about individual measurements


# **GetMeasurements**
> Measurements GetMeasurements (string country = null, string city = null, string location = null, string parameter = null, bool? hasGeo = null, string coordinates = null, float? radius = null, float? valueFrom = null, float? valueTo = null, string dateFrom = null, string dateTo = null, string orderBy = null, string sort = null, List<string> includeFields = null, float? limit = null, float? page = null, string format = null)

Provides data about individual measurements

Provides data about individual measurements

### Example
```csharp
using System;
using System.Diagnostics;
using Org.Openaq.Api.Openaq.Api;
using Org.Openaq.Api.Openaq.Client;
using Org.Openaq.Api.Openaq.Model;

namespace Example
{
    public class GetMeasurementsExample
    {
        public void main()
        {
            
            var apiInstance = new MeasurementsApi();
            var country = ;  // string |     Limit results by a certain country.     (optional) 
            var city = ;  // string |     Limit results by a certain city.     (optional) 
            var location = ;  // string |     Limit results by a certain location.     (optional) 
            var parameter = ;  // string |     Limit to certain one or more parameters (ex. parameter=pm25 or parameter[]=co&parameter[]=pm25)     Valores permitidos: pm25, pm10, so2, no2, o3, co, bc     (optional) 
            var hasGeo = ;  // bool? |     Filter out items that have or do not have geographic information.     Valores permitidos: true, false     (optional) 
            var coordinates = ;  // string |     Center point (lat, lon) used to get measurements within a certain area. (ex. coordinates=40.23,34.17)     (optional) 
            var radius = ;  // float? |     Radius (in meters) used to get measurements within a certain area, must be used with coordinates.     Valor por defecto: 2500     (optional)  (default to 2500.0)
            var valueFrom = ;  // float? |     Show results above value threshold, useful in combination with parameter.     (optional) 
            var valueTo = ;  // float? |     Show results below value threshold, useful in combination with parameter.     (optional) 
            var dateFrom = ;  // string |     Show results after a certain date. This acts on the utc timestamp of each measurement. (ex. 2015-12-20, or 2015-12-20T09:00:00)     (optional) 
            var dateTo = ;  // string |     Show results before a certain date. This acts on the utc timestamp of each measurement. (ex. 2015-12-20, or 2015-12-20T09:00:00)     (optional) 
            var orderBy = ;  // string |     Order by one or more fields (ex. order_by=date or order_by[]=value&order_by[]=parameter).     Valor por defecto: date     (optional)  (default to date)
            var sort = ;  // string |     Define sort order for one or more fields (ex. sort=desc or sort[]=asc&sort[]=desc).     Valor por defecto: asc     (optional)  (default to asc)
            var includeFields = new List<string>(); // List<string> |     Include extra fields in the output in addition to default values.     Valores permitidos: attribution, averagingPeriod, sourceName     (optional) 
            var limit = ;  // float? |     Change the number of results returned, max is 10000.     Valor por defecto: 100     (optional)  (default to 100.0)
            var page = ;  // float? |     Paginate through results.     Valor por defecto: 1     (optional)  (default to 1.0)
            var format = ;  // string |     Format for data return. Note that csv will return a max of 65,536 results when no limit is set.     Valor por defecto: json     Valores permitidos: csv, json     (optional)  (default to json)

            try
            {
                // Provides data about individual measurements
                Measurements result = apiInstance.GetMeasurements(country, city, location, parameter, hasGeo, coordinates, radius, valueFrom, valueTo, dateFrom, dateTo, orderBy, sort, includeFields, limit, page, format);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MeasurementsApi.GetMeasurements: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **country** | **string**|     Limit results by a certain country.     | [optional] 
 **city** | **string**|     Limit results by a certain city.     | [optional] 
 **location** | **string**|     Limit results by a certain location.     | [optional] 
 **parameter** | **string**|     Limit to certain one or more parameters (ex. parameter&#x3D;pm25 or parameter[]&#x3D;co&amp;parameter[]&#x3D;pm25)     Valores permitidos: pm25, pm10, so2, no2, o3, co, bc     | [optional] 
 **hasGeo** | **bool?**|     Filter out items that have or do not have geographic information.     Valores permitidos: true, false     | [optional] 
 **coordinates** | **string**|     Center point (lat, lon) used to get measurements within a certain area. (ex. coordinates&#x3D;40.23,34.17)     | [optional] 
 **radius** | **float?**|     Radius (in meters) used to get measurements within a certain area, must be used with coordinates.     Valor por defecto: 2500     | [optional] [default to 2500.0]
 **valueFrom** | **float?**|     Show results above value threshold, useful in combination with parameter.     | [optional] 
 **valueTo** | **float?**|     Show results below value threshold, useful in combination with parameter.     | [optional] 
 **dateFrom** | **string**|     Show results after a certain date. This acts on the utc timestamp of each measurement. (ex. 2015-12-20, or 2015-12-20T09:00:00)     | [optional] 
 **dateTo** | **string**|     Show results before a certain date. This acts on the utc timestamp of each measurement. (ex. 2015-12-20, or 2015-12-20T09:00:00)     | [optional] 
 **orderBy** | **string**|     Order by one or more fields (ex. order_by&#x3D;date or order_by[]&#x3D;value&amp;order_by[]&#x3D;parameter).     Valor por defecto: date     | [optional] [default to date]
 **sort** | **string**|     Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc).     Valor por defecto: asc     | [optional] [default to asc]
 **includeFields** | [**List<string>**](string.md)|     Include extra fields in the output in addition to default values.     Valores permitidos: attribution, averagingPeriod, sourceName     | [optional] 
 **limit** | **float?**|     Change the number of results returned, max is 10000.     Valor por defecto: 100     | [optional] [default to 100.0]
 **page** | **float?**|     Paginate through results.     Valor por defecto: 1     | [optional] [default to 1.0]
 **format** | **string**|     Format for data return. Note that csv will return a max of 65,536 results when no limit is set.     Valor por defecto: json     Valores permitidos: csv, json     | [optional] [default to json]

### Return type

[**Measurements**](Measurements.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

