# Org.Openaq.Api.Openaq.Model.Sources
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Active** | **bool?** |  | 
**Adapter** | **string** |  | [optional] 
**City** | **string** |  | [optional] 
**Contacts** | **List&lt;string&gt;** |  | [optional] 
**Country** | **string** |  | [optional] 
**Description** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**Resolution** | **string** |  | [optional] 
**SourceURL** | **string** |  | [optional] 
**Url** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

