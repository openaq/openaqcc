# Org.Openaq.Api.Openaq.Api.FetchesApi

All URIs are relative to *https://api.openaq.org/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetFectches**](FetchesApi.md#getfectches) | **GET** /fetches | Providing data about individual fetch operations that are used to populate data in the platform.


# **GetFectches**
> Fetch GetFectches (List<string> orderBy = null, List<string> sort = null, int? limit = null, int? page = null)

Providing data about individual fetch operations that are used to populate data in the platform.

Providing data about individual fetch operations that are used to populate data in the platform.

### Example
```csharp
using System;
using System.Diagnostics;
using Org.Openaq.Api.Openaq.Api;
using Org.Openaq.Api.Openaq.Client;
using Org.Openaq.Api.Openaq.Model;

namespace Example
{
    public class GetFectchesExample
    {
        public void main()
        {
            
            var apiInstance = new FetchesApi();
            var orderBy = new List<string>(); // List<string> | order_by (optional) 
            var sort = new List<string>(); // List<string> | Define sort order for one or more fields (ex. sort=desc or sort[]=asc&sort[]=desc). (optional)  (default to asc)
            var limit = ;  // int? | Change the number of results returned (optional)  (default to 100)
            var page = ;  // int? | Paginate through results. (optional)  (default to 1)

            try
            {
                // Providing data about individual fetch operations that are used to populate data in the platform.
                Fetch result = apiInstance.GetFectches(orderBy, sort, limit, page);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FetchesApi.GetFectches: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderBy** | [**List<string>**](string.md)| order_by | [optional] 
 **sort** | [**List<string>**](string.md)| Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc). | [optional] [default to asc]
 **limit** | **int?**| Change the number of results returned | [optional] [default to 100]
 **page** | **int?**| Paginate through results. | [optional] [default to 1]

### Return type

[**Fetch**](Fetch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

