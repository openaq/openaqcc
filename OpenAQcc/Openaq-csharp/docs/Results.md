# Org.Openaq.Api.Openaq.Model.Results
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**City** | **string** |  | 
**Count** | **int?** |  | [optional] 
**Country** | **string** |  | [optional] 
**Location** | **string** |  | [optional] 
**Measurements** | [**List&lt;Measurements&gt;**](Measurements.md) |  | [optional] 
**_Results** | [**List&lt;Results2&gt;**](Results2.md) |  | [optional] 
**TimeEnded** | **DateTime?** |  | [optional] 
**TimeStarted** | **DateTime?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

