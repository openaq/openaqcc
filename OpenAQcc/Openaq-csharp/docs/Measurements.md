# Org.Openaq.Api.Openaq.Model.Measurements
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AveragingPeriod** | [**AveragingPeriod**](AveragingPeriod.md) |  | 
**LastUpdated** | **DateTime?** |  | [optional] 
**Parameter** | **string** |  | [optional] 
**SourceName** | **string** |  | [optional] 
**Unit** | **string** |  | [optional] 
**Value** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

