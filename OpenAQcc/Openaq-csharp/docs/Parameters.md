# Org.Openaq.Api.Openaq.Model.Parameters
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Description** | **string** |  | 
**Id** | **string** |  | [optional] 
**Name** | **string** |  | [optional] 
**PreferredUnit** | **string** |  | [optional] 
**Results** | [**List&lt;Results&gt;**](Results.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

