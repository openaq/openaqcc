# Org.Openaq.Api.Openaq.Model.Responses
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Results** | [**List&lt;Results&gt;**](Results.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

