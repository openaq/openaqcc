# Org.Openaq.Api.Openaq.Api.SourcesApi

All URIs are relative to *https://api.openaq.org/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetSources**](SourcesApi.md#getsources) | **GET** /sources | Provides a list of data sources.


# **GetSources**
> List<Sources> GetSources (List<string> orderBy = null, string sort = null, float? limit = null, float? page = null)

Provides a list of data sources.

Provides a list of data sources.

### Example
```csharp
using System;
using System.Diagnostics;
using Org.Openaq.Api.Openaq.Api;
using Org.Openaq.Api.Openaq.Client;
using Org.Openaq.Api.Openaq.Model;

namespace Example
{
    public class GetSourcesExample
    {
        public void main()
        {
            
            var apiInstance = new SourcesApi();
            var orderBy = new List<string>(); // List<string> | Order by one or more fields (ex. order_by=country or order_by[]=active&order_by[]=country). (optional)  (default to name)
            var sort = ;  // string | Define sort order for one or more fields (ex. sort=desc or sort[]=asc&sort[]=desc) (optional)  (default to asc)
            var limit = ;  // float? | Change the number of results returned (optional)  (default to 100.0)
            var page = ;  // float? | Paginate through results. (optional)  (default to 1.0)

            try
            {
                // Provides a list of data sources.
                List&lt;Sources&gt; result = apiInstance.GetSources(orderBy, sort, limit, page);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SourcesApi.GetSources: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderBy** | [**List<string>**](string.md)| Order by one or more fields (ex. order_by&#x3D;country or order_by[]&#x3D;active&amp;order_by[]&#x3D;country). | [optional] [default to name]
 **sort** | **string**| Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc) | [optional] [default to asc]
 **limit** | **float?**| Change the number of results returned | [optional] [default to 100.0]
 **page** | **float?**| Paginate through results. | [optional] [default to 1.0]

### Return type

[**List<Sources>**](Sources.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

