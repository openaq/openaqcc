# Org.Openaq.Api.Openaq.Api.LocationsApi

All URIs are relative to *https://api.openaq.org/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetLocations**](LocationsApi.md#getlocations) | **GET** /locations | Provides a list of measurement locations and their meta data.


# **GetLocations**
> List<Locations> GetLocations (string city = null, string country = null, string location = null, string parameter = null, bool? hasGeo = null, string coordinates = null, float? nearest = null, float? radius = null, List<string> orderBy = null, List<string> sort = null, float? limit = null, float? page = null)

Provides a list of measurement locations and their meta data.

Provides a list of measurement locations and their meta data.

### Example
```csharp
using System;
using System.Diagnostics;
using Org.Openaq.Api.Openaq.Api;
using Org.Openaq.Api.Openaq.Client;
using Org.Openaq.Api.Openaq.Model;

namespace Example
{
    public class GetLocationsExample
    {
        public void main()
        {
            
            var apiInstance = new LocationsApi();
            var city = ;  // string | Limit results by one or more cities (ex. city[]=Lisboa&city[]=Porto)     (optional) 
            var country = ;  // string | Limit results by one or more countries (ex. country[]=NL&country[]=PL)     (optional) 
            var location = ;  // string | Limit results by one or more locations (ex. location[]=Reja&location[]=Nijmegen-Graafseweg)     (optional) 
            var parameter = ;  // string | Limit to certain one or more parameters (ex. parameter=pm25 or parameter[]=co&parameter[]=pm25)      (optional) 
            var hasGeo = ;  // bool? | Filter out items that have or do not have geographic information.     (optional) 
            var coordinates = ;  // string | Center point (lat, lon) used to get locations within/near a certain area. (ex. coordinates=40.23,34.17)     (optional) 
            var nearest = ;  // float? | Get the X nearest locations to coordinates, must be used with coordinates. Wins over radius if both are present. Will add distance property to locations. DEPRECATED: Use order_by=distance with limit=X instead.     (optional) 
            var radius = ;  // float? | Radius (in meters) used to get locations within a certain area, must be used with coordinates.  (optional)  (default to 2500.0)
            var orderBy = new List<string>(); // List<string> | Order by one or more fields (ex. order_by=count or order_by[]=country&order_by[]=count). (optional)  (default to location)
            var sort = new List<string>(); // List<string> | Define sort order for one or more fields (ex. sort=desc or sort[]=asc&sort[]=desc).     Valor por defecto: asc     (optional) 
            var limit = ;  // float? | Change the number of results returned     Valor por defecto: 100     (optional)  (default to 100.0)
            var page = ;  // float? | Paginate through results.    (optional)  (default to 1.0)

            try
            {
                // Provides a list of measurement locations and their meta data.
                List&lt;Locations&gt; result = apiInstance.GetLocations(city, country, location, parameter, hasGeo, coordinates, nearest, radius, orderBy, sort, limit, page);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling LocationsApi.GetLocations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **city** | **string**| Limit results by one or more cities (ex. city[]&#x3D;Lisboa&amp;city[]&#x3D;Porto)     | [optional] 
 **country** | **string**| Limit results by one or more countries (ex. country[]&#x3D;NL&amp;country[]&#x3D;PL)     | [optional] 
 **location** | **string**| Limit results by one or more locations (ex. location[]&#x3D;Reja&amp;location[]&#x3D;Nijmegen-Graafseweg)     | [optional] 
 **parameter** | **string**| Limit to certain one or more parameters (ex. parameter&#x3D;pm25 or parameter[]&#x3D;co&amp;parameter[]&#x3D;pm25)      | [optional] 
 **hasGeo** | **bool?**| Filter out items that have or do not have geographic information.     | [optional] 
 **coordinates** | **string**| Center point (lat, lon) used to get locations within/near a certain area. (ex. coordinates&#x3D;40.23,34.17)     | [optional] 
 **nearest** | **float?**| Get the X nearest locations to coordinates, must be used with coordinates. Wins over radius if both are present. Will add distance property to locations. DEPRECATED: Use order_by&#x3D;distance with limit&#x3D;X instead.     | [optional] 
 **radius** | **float?**| Radius (in meters) used to get locations within a certain area, must be used with coordinates.  | [optional] [default to 2500.0]
 **orderBy** | [**List<string>**](string.md)| Order by one or more fields (ex. order_by&#x3D;count or order_by[]&#x3D;country&amp;order_by[]&#x3D;count). | [optional] [default to location]
 **sort** | [**List<string>**](string.md)| Define sort order for one or more fields (ex. sort&#x3D;desc or sort[]&#x3D;asc&amp;sort[]&#x3D;desc).     Valor por defecto: asc     | [optional] 
 **limit** | **float?**| Change the number of results returned     Valor por defecto: 100     | [optional] [default to 100.0]
 **page** | **float?**| Paginate through results.    | [optional] [default to 1.0]

### Return type

[**List<Locations>**](Locations.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

