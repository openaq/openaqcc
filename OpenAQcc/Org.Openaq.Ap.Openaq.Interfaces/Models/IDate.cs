using System;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface IDate : IEquatable<IDate>
    {
        /// <summary>
        /// Gets or Sets Found
        /// </summary>
        DateTime? Utc { get; set; }

        /// <summary>
        /// Gets or Sets License
        /// </summary>
        DateTime? Local { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}