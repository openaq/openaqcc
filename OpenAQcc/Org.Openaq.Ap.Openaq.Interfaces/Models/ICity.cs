using System;
using System.Runtime.Serialization;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    
    public interface ICity : IEquatable<ICity>
    {
        /// <summary>
        /// Gets or Sets _City
        /// </summary>
        [DataMember(Name = "city", EmitDefaultValue = false)]
        string Name { get; set; }

        /// <summary>
        /// Gets or Sets Count
        /// </summary>
        [DataMember(Name = "count", EmitDefaultValue = false)]
        int? Count { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        [DataMember(Name = "country", EmitDefaultValue = false)]
        string CountryCode { get; set; }

        /// <summary>
        /// Gets or Sets Locations
        /// </summary>
        [DataMember(Name = "locations", EmitDefaultValue = false)]
        int? Locations { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}