using System;
using System.Collections.Generic;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface ISources : IEquatable<ISources>
    {
        /// <summary>
        /// Gets or Sets Active
        /// </summary>
        bool? Active { get; set; }

        /// <summary>
        /// Gets or Sets Adapter
        /// </summary>
        string Adapter { get; set; }

        /// <summary>
        /// Gets or Sets City
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Gets or Sets Contacts
        /// </summary>
        List<string> Contacts { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        string Country { get; set; }

        /// <summary>
        /// Gets or Sets Description
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or Sets Resolution
        /// </summary>
        string Resolution { get; set; }

        /// <summary>
        /// Gets or Sets SourceURL
        /// </summary>
        string SourceUrl { get; set; }

        /// <summary>
        /// Gets or Sets Url
        /// </summary>
        string Url { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}