using System;
using System.Collections.Generic;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface ILocation : IEquatable<ILocation>
    {
        /// <summary>
        /// Gets or Sets City
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Gets or Sets Count
        /// </summary>
        int? Count { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        string Country { get; set; }

        /// <summary>
        /// Gets or Sets FirstUpdated
        /// </summary>
        DateTime? FirstUpdated { get; set; }

        /// <summary>
        /// Gets or Sets LastUpdated
        /// </summary>
        DateTime? LastUpdated { get; set; }

        /// <summary>
        /// Gets or Sets Location
        /// </summary>
        string Locations { get; set; }

        /// <summary>
        /// Gets or Sets Parameters
        /// </summary>
        List<string> Parameters { get; set; }

        /// <summary>
        /// Gets or Sets SourceName
        /// </summary>
        string SourceName { get; set; }

        List<String> SourceNames { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}