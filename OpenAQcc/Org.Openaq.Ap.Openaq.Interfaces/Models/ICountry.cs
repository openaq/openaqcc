﻿using System;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface ICountry : IEquatable<ICountry>
    {
        /// <summary>
        /// Gets or Sets Code
        /// </summary>
        string Code { get; set; }

        /// <summary>
        /// Gets or Sets Count
        /// </summary>
        int? Count { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}
