﻿using System;
using System.Collections.Generic;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface IFetchSummary : IEquatable<IFetchSummary>
    {
        /// <summary>
        /// Gets or Sets Count
        /// </summary>
        int? Count { get; set; }

        /// <summary>
        /// Gets or Sets Duration
        /// </summary>
        List<IFetch> Fetches { get; set; }

        /// <summary>
        /// Gets or Sets Failures
        /// </summary>
        DateTime? TimeStarted { get; set; }

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        DateTime? TimeEnded { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}
