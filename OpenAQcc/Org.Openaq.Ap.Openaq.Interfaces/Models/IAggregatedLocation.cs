using System;
using System.Collections.Generic;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface IAggregatedLocation : IEquatable<IAggregatedLocation>
    {
        /// <summary>
        /// Gets or Sets City
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        string Country { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        double Distance { get; set; }

        /// <summary>
        /// Gets or Sets Location
        /// </summary>
        string Location { get; set; }

        /// <summary>
        /// Gets or Sets Parameters
        /// </summary>
        List<IAggregatedMeasurement> Measurements { get; set; }

        ICoordinates Coordinates { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}