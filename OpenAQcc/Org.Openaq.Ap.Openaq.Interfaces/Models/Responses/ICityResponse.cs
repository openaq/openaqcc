using System.Collections.Generic;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models.Responses
{
    public interface ICityResponse
    {
        IMeta Metadata { get; set; }

        /// <summary>
        /// Gets or Sets Results
        /// </summary>
        List<ICity> Results { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        string ToString();

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();

      

        

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        int GetHashCode();
    }
}