using System.Collections.Generic;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models.Responses
{
    public interface ISourcesResponse
    {
        IMeta Metadata { get; set; }

        /// <summary>
        /// Gets or Sets Results
        /// </summary>
        List<ISources> Results { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        string ToString();

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        bool Equals(object obj);

        /// <summary>
        /// Returns true if Responses instances are equal
        /// </summary>
        /// <param name="other">Instance of Responses to be compared</param>
        /// <returns>Boolean</returns>
        bool Equals(ISourcesResponse other);

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        int GetHashCode();
    }
}