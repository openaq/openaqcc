using System;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface ICoordinates : IEquatable<ICoordinates>
    {
        /// <summary>
        /// Gets or Sets Latitude
        /// </summary>
        float? Latitude { get; set; }

        /// <summary>
        /// Gets or Sets Longitude
        /// </summary>
        float? Longitude { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}