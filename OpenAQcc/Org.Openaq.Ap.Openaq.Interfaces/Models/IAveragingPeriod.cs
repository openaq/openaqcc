﻿using System;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface IAveragingPeriod : IEquatable<IAveragingPeriod>
    {
        /// <summary>
        /// Gets or Sets Unit
        /// </summary>
        string Unit { get; set; }

        /// <summary>
        /// Gets or Sets Value
        /// </summary>
        float? Value { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}
