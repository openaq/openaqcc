﻿using System;
using System.Collections.Generic;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface IFetch : IEquatable<IFetch>
    {
        /// <summary>
        /// Gets or Sets Count
        /// </summary>
        int? Count { get; set; }

        /// <summary>
        /// Gets or Sets Duration
        /// </summary>
        float? Duration { get; set; }

        /// <summary>
        /// Gets or Sets Failures
        /// </summary>
        Dictionary<string, int> Failures { get; set; }

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// Gets or Sets SourceName
        /// </summary>
        string SourceName { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}
