using System;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface IMeasurement : IEquatable<IMeasurement>
    {
        /// <summary>
        /// Gets or Sets City
        /// </summary>
        string City { get; set; }

        /// <summary>
        /// Gets or Sets Coordinates
        /// </summary>
        ICoordinates Coordinates { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        string Country { get; set; }

        /// <summary>
        /// Gets or Sets Date
        /// </summary>
        IDate Date { get; set; }

        /// <summary>
        /// Gets or Sets Location
        /// </summary>
        string Location { get; set; }

        /// <summary>
        /// Gets or Sets Parameter
        /// </summary>
        string Parameter { get; set; }

        /// <summary>
        /// Gets or Sets SourceName
        /// </summary>
        string SourceName { get; set; }

        /// <summary>
        /// Gets or Sets Unit
        /// </summary>
        string Unit { get; set; }

        /// <summary>
        /// Gets or Sets Value
        /// </summary>
        string Value { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}