using System;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface IAggregatedMeasurement : IEquatable<IAggregatedMeasurement>
    {
        /// <summary>
        /// Gets or Sets AveragingPeriod
        /// </summary>
        IAveragingPeriod AveragingPeriod { get; set; }

        DateTime? LastUpdated { get; set; }

        /// <summary>
        /// Gets or Sets SourceName
        /// </summary>
        string SourceName { get; set; }

        string Parameter { get; set; }

        /// <summary>
        /// Gets or Sets Unit
        /// </summary>
        string Unit { get; set; }

        /// <summary>
        /// Gets or Sets Value
        /// </summary>
        string Value { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}