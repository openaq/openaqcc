using System;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface IAqParameter : IEquatable<IAqParameter>
    {
        /// <summary>
        /// Gets or Sets Description
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or Sets PreferredUnit
        /// </summary>
        string PreferredUnit { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();

        string Id { get; set; }
    }
}
