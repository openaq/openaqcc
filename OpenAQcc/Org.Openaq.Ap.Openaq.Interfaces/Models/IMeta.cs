using System;

namespace Org.Openaq.Ap.Openaq.Interfaces.Models
{
    public interface IMeta : IEquatable<IMeta>
    {
        /// <summary>
        /// Gets or Sets Found
        /// </summary>
        int? Found { get; set; }

        /// <summary>
        /// Gets or Sets License
        /// </summary>
        string License { get; set; }

        /// <summary>
        /// Gets or Sets Limit
        /// </summary>
        int? Limit { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or Sets Page
        /// </summary>
        int? Page { get; set; }

        /// <summary>
        /// Gets or Sets Website
        /// </summary>
        string Website { get; set; }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        string ToJson();
    }
}