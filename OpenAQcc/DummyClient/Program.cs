﻿using System;
using System.Configuration;
using Org.Openaq.Ap.Openaq.Interfaces.Api;
using Org.Openaq.Api.Openaq.Api;

namespace DummyClient
{
    class Program
    {
        private const string ApiUrlConfigKey = "ApiUrl";
        static void Main(string[] args)
        {
            IProxy oaproxy = new Proxy(ConfigurationManager.AppSettings.Get(ApiUrlConfigKey));
            
            var response = oaproxy.GetCities();
            Console.WriteLine("Cities");
            Console.ForegroundColor = ConsoleColor.Yellow;
            foreach (var item in response.Results)
            {
                Console.WriteLine(item.ToString());
            }
            Console.Read();
            response = oaproxy.GetCities("ES");
            Console.WriteLine("Spanish cities");
            Console.ForegroundColor = ConsoleColor.Blue;
            foreach (var item in response.Results)
            {
                Console.WriteLine(item.ToString());
            }

            Console.Read();
            var responsecountry = oaproxy.GetCountries();
            Console.WriteLine("Countries");
            Console.ForegroundColor = ConsoleColor.Green;
            foreach (var item in responsecountry.Results)
            {
                Console.WriteLine(item.ToString());
            }
            Console.Read();
            var responsefetches = oaproxy.GetFectches();
            Console.WriteLine("Fetches");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            foreach (var item in responsefetches.Results)
            {
                Console.WriteLine(item.ToString());
            }
            Console.Read();
            var responselatets = oaproxy.GetLatestParameters();
            Console.WriteLine("latest parameters");
            Console.ForegroundColor = ConsoleColor.Red;
            foreach (var item in responselatets.Results)
            {
                Console.WriteLine(item.ToString());
            }
            Console.Read();
            var responsLocations = oaproxy.GetLocations();
            Console.WriteLine("Locations");
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var item in responsLocations.Results)
            {
                Console.WriteLine(item.ToString());
            }
            Console.Read();
            var responsMeasurements = oaproxy.GetMeasurements();
            Console.WriteLine("Measurements");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            foreach (var item in responsMeasurements.Results)
            {
                Console.WriteLine(item.ToString());
            }
            Console.Read();
            var responsparameters = oaproxy.Getparameters();
            Console.WriteLine("Parameters");
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            foreach (var item in responsparameters.Results)
            {
                Console.WriteLine(item.ToString());
            }
            Console.Read();

            var responsSources = oaproxy.GetSources();
            Console.WriteLine("Sources");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            foreach (var item in responsSources.Results)
            {
                Console.WriteLine(item.ToString());
            }

            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("DONE");
            Console.Read();
        }
    }
}
