﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jmelosegui.Mvc.GoogleMap;
using Org.Openaq.Ap.Openaq.Interfaces.Api;
using Org.Openaq.Api.Openaq.Api;

namespace Map.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            IProxy openairproxy = new Proxy("https://api.openaq.org/v1");
            var response = openairproxy.GetLatestParameters(country: "ES", limit:1000,hasGeo:true);
            if(response!= null)
            {
                IList<Marker> markers = new List<Marker>();
                foreach (var location in response.Results)
                {
                    foreach (var m in location.Measurements)
                    {
                        
                    }
                }
            }

            
            return View(response.Results);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
    }
}